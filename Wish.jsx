
import React, { useState, useEffect } from "react";

const Planets = () => {
  const [hasError, setErrors] = useState(false);
  const [planets, setPlanets] = useState({});

  useEffect(() =>
    fetch("https://api.jsonbin.io/b/5efdf1000bab551d2b6ab1c9/1")
      .then(res => res.json())
      .then(res => this.setState({ planets: res }))
      .catch(() => this.setState({ hasErrors: true }))
  );

  return <div>{JSON.stringify(planets)}</div>;
};
export default Planets;